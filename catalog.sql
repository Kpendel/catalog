-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Дек 28 2013 г., 22:26
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `catalog`
--

-- --------------------------------------------------------

--
-- Структура таблицы `audio`
--

CREATE TABLE IF NOT EXISTS `audio` (
  `id_audio` int(11) NOT NULL AUTO_INCREMENT,
  `src` varchar(100) NOT NULL,
  `composer` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `id_genre` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `id_author` int(11) NOT NULL,
  `album` varchar(45) NOT NULL,
  `description` varchar(200) NOT NULL,
  `year` varchar(4) NOT NULL,
  PRIMARY KEY (`id_audio`),
  KEY `id_genre_idx` (`id_genre`),
  KEY `id_author_idx` (`id_author`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `audio`
--

INSERT INTO `audio` (`id_audio`, `src`, `composer`, `name`, `id_genre`, `rating`, `id_author`, `album`, `description`, `year`) VALUES
(1, '05 Hive (feat. Vince Staples and Casey Veggies).mp3', 'Earl Sweatshirt', 'Hive', 1, 1, 1, 'Doris', 'earl sweatshirt is a young rapper form LA', '2013'),
(2, '01 Dani California.mp3', 'Red Hot Chili Peppers', 'Dani California', 2, 1, 1, 'Stadium Arcadium', 'the best song from RHCP', '2006'),
(3, '11 Guild (feat. Mac Miller).mp3', 'Earl Sweatshirt feat. Mac Miller', 'Guild', 1, 7, 1, 'Doris', 'blablabla', '2013');

-- --------------------------------------------------------

--
-- Структура таблицы `author`
--

CREATE TABLE IF NOT EXISTS `author` (
  `id_author` int(11) NOT NULL AUTO_INCREMENT,
  `author_name` varchar(45) NOT NULL,
  `password` varchar(200) NOT NULL,
  PRIMARY KEY (`id_author`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `author`
--

INSERT INTO `author` (`id_author`, `author_name`, `password`) VALUES
(1, 'admin', 'c3284d0f94606de1fd2af172aba15bf3');

-- --------------------------------------------------------

--
-- Структура таблицы `genre`
--

CREATE TABLE IF NOT EXISTS `genre` (
  `idgenre` int(11) NOT NULL AUTO_INCREMENT,
  `genre_name` varchar(45) NOT NULL,
  PRIMARY KEY (`idgenre`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `genre`
--

INSERT INTO `genre` (`idgenre`, `genre_name`) VALUES
(1, 'Хип-Хоп'),
(2, 'Рок'),
(3, 'Поп');

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `audio`
--
ALTER TABLE `audio`
  ADD CONSTRAINT `id_author` FOREIGN KEY (`id_author`) REFERENCES `author` (`id_author`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `id_genre` FOREIGN KEY (`id_genre`) REFERENCES `genre` (`idgenre`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
